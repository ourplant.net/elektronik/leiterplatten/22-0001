# 22-0001
***
3D-Kopf LP2 (Beleuchtung)

State: **active**

![Abbildung 3D-Kopf LP2 (Beleuchtung)](https://gitlab.com/ourplant.net/elektronik/leiterplatten/22-0001/-/raw/master/22-0001-AVT-A.png)

## Brief
Lighting board consisting of 12 LEDs. 6 LEDs are connected in series with a common cathode
<!---
Beleuchtungspaltine bestehend aus 12 LEDs. Es sind jeweils 6 LEDs in Reihe mit gemeinsamer Kathode geschaltet.
-->

## Overviev
Data    | Description
-------- | -------- 
Revision   | A  
Product Type | Lighting 
Features	| Lighting board, LED colour : red
Connectors | 2x Anoden, 1 Kathode 
Examples | Kamera lighting
Dimension | 37 x 35 mm
Layer	| 2
Supply | 2x 12V@20mA 
Wavelength | 633 nm
Notice 	| Two series resistors (670Ohm 0.5W) are required for operation at 24V
<!---
Für den Betrieb an 24V werden zwei Vorwidertände benötigt
-->

## Proof of use
Item  | Name
----- | ------
06-0310.03 | Service SIG Sonneborn 
20-0084 | Beleuchtung
20-0284 | 3D-Kamerakopf
20-0314 | 3D-Kamera
20-0444 | 3D-Kamera RBHH
S8-0024 | Beleuchtung

## Description
The circuit board is glued to the lighting fixture. The connection is made via 3 wires that are soldered on.

## Partlist
[Partlist](CAD/Eagle/partlist.md)
