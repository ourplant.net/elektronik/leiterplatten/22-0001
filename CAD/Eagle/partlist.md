quantity | unit | name | article | supplier | order number | parts
-------- | ---- | ---- | ------- | -------- | ------------ | -----
1 | Stk | 3D-Kopf LP2 PCB 2-stg PT  12 x 16 2xLSL e-test | 50-0003 | Würth (09-1434.02) Leiterplatten | None | 
. |   |  |  |  Beta LAYOUT GmbH | 50-0003 | 
12 | Stk | SMD LED rot LST676-PS | 70-0265 | Conrad Electronic | 519954 - 62  | 
. |   |  |  | RS (HL) | 238-2219 | 
. |   |  |  | Mouser Electronics (ab 65 €) | 720-LST676-P2S1-1-Z  | 
